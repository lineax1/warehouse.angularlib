import { Component } from '@angular/core';
import { UserView } from '../model/user/user.view';
import { UserService } from '../services/user.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent {
  public user: UserView;
  public isRequesting: boolean;
  private submitted = true;

  constructor(
    private registrationServices: UserService,
    private toastr: ToastrService,
    private router: Router,
    ) { }

  public onSubmit({ value, valid }) {
    if (!valid) {
      return;
    }
    this.submitted = true;
    this.isRequesting = true;
    this.user = value;

    this.registrationServices.userRegistration(value).subscribe(
      result => {
        console.log(result);
        this.toastr.success('Successful registration ');
      },
      errors => {
        console.log(errors);
      },

      () =>
        this.router.navigate(['/login'], {
          queryParams: { brandNew: true, email: value.email }
        })
    );
  }
}
