import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MenuComponent } from '../menu/menu.component';
import { MatMenuModule } from '@angular/material/menu';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';



@NgModule({
  declarations: [
  MenuComponent
  ],
imports:[
  RouterModule,
  BrowserModule,
  BrowserAnimationsModule,
  FormsModule,
  MDBBootstrapModule,
  MatCardModule,
  MatDialogModule,
  MatMenuModule,
  ReactiveFormsModule,
  HttpClientModule,
  ToastrModule.forRoot({
    timeOut: 10000,
    positionClass: 'toast-bottom-right',
    preventDuplicates: true,
  }),
],
  exports: [MenuComponent, RouterModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MDBBootstrapModule,
    MatCardModule,
    MatDialogModule,
    MatMenuModule,
    ReactiveFormsModule,
    HttpClientModule,
   ]
})
export class SharedModule { }
