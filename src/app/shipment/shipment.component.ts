import { Component, OnInit } from '@angular/core';
import { ShipmentView } from '../model/shipment/shipment.view';
import { ShipmentService } from '../services/shipment.service';

@Component({
  selector: 'app-shipment',
  templateUrl: './shipment.component.html',
  styleUrls: ['./shipment.component.css']
})
export class ShipmentComponent implements OnInit {
  public shipmentList: ShipmentView[] = [];

  constructor(private shipmentService: ShipmentService) { }

  ngOnInit(): void {
    this.getAll();
  }

  public getAll(): void {
    this.shipmentService.getAll().subscribe((res: ShipmentView[]) => {
      this.shipmentList = res;
    });
  }
}
