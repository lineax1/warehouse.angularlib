import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { OrderView } from '../model/order/order.view';

@Injectable()
export class OrderService {

  constructor(private http: HttpClient) { }

  public getById(id: number) {
    return this.http.get<any>(environment.apiUrl + '/api/order/getbyid' + id);
  }

  public getAll(): Observable<OrderView[]> {
    return this.http.get<OrderView[]>(environment.apiUrl + '/api/order/getall');
  }

  public create(item: OrderView): Observable<OrderView> {
    return this.http.post<OrderView>(environment.apiUrl + '/api/order/create', item);
  }
  public delete(id: number) {
    return this.http.delete(environment.apiUrl + '/api/order/delete?id=' + id);
  }
}
