import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AccountView } from '../model/account/account.view';
import { environment } from 'src/environments/environment';

@Injectable()
export class AccountService {
  constructor(private http: HttpClient) { }

  public getById(id: number) {
    return this.http.get<any>(environment.apiUrl + '/api/account/getbyid?id=' + id);
  }

  public getByEmail(email: string): Observable<any> {
    return this.http.post<any>(environment.apiUrl + '/api/account/getbyemail', {email});
  }

  public create(item: AccountView): Observable<AccountView> {
    return this.http.post<AccountView>(environment.apiUrl + '/api/account/create', item);
  }

  public update(item: AccountView): Observable<AccountView> {
    return this.http.put<AccountView>(environment.apiUrl + '/api/account/update', item);
  }

  public uploadImage(data: FormData): Observable<any> {
    return this.http.post<string>(environment.apiUrl + '/api/account/upload', data );
  }
}
