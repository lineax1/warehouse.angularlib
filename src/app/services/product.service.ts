import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ProductView } from '../model/product/product.view';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class ProductService {

  constructor(private readonly http: HttpClient) { }

  public getById(id: number): Observable<ProductView> {
    return this.http.get<ProductView>(environment.apiUrl + '/api/product/getbyid?id=' + id);
  }
  public getAll(): Observable<ProductView[]> {
    return this.http.get<ProductView[]>(environment.apiUrl + '/api/product/getall');
  }

  public create(createItem: ProductView): Observable<ProductView> {
    return this.http.post<ProductView>(environment.apiUrl + '/api/product/create', createItem);
  }

  public update(updateItem: ProductView): Observable<ProductView> {
    return this.http.put<ProductView>(environment.apiUrl + '/api/product/update', updateItem);
  }

  public delete(id: number) {
    return this.http.delete(environment.apiUrl + '/api/product/delete/?id=' + id);
  }
}
