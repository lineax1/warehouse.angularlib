import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ShipmentView } from '../model/shipment/shipment.view';
import { environment } from 'src/environments/environment';

@Injectable()
export class ShipmentService {
  constructor(private http: HttpClient) { }

  public getAll(): Observable<ShipmentView[]> {
    return this.http.get<ShipmentView[]>(environment.apiUrl + '/api/shipment/getall');
  }

  public create(createItem: ShipmentView): Observable<ShipmentView> {
    return this.http.post<ShipmentView>(environment.apiUrl + '/api/shipment/create', createItem);
  }

  public update(updateItem: ShipmentView): Observable<ShipmentView> {
    return this.http.put<ShipmentView>(environment.apiUrl + '/api/shipment/update', updateItem);
  }

  public delete(id?: number) {
    return this.http.delete(environment.apiUrl + '/api/shipment/delete?id=' + id);
  }
}
