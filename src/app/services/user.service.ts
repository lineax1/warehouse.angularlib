import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserView } from '../model/user/user.view';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';
import { Router } from '@angular/router';


@Injectable()
export class UserService {
  private isAdmin: boolean = false;
  constructor(
    private http: HttpClient,
    private router: Router
    ) {
      this.getIsAdminFromLocalStorage();
     }

  public userRegistration(user: UserView): Observable<UserView> {
    return this.http.post<UserView>(environment.apiUrl + '/api/user/registration', user);
  }

  public login(user: UserView) {
    return this.http.post<any>(environment.apiUrl + '/api/user/login', user)
      .pipe(map(mapuser => {
        localStorage.setItem('token', JSON.stringify(mapuser.token));
        localStorage.setItem('role', mapuser.role);
        localStorage.setItem('accountId', mapuser.accountId);
        return mapuser;
      }));
  }

  getAuthToken(): string {
    const currentUser = JSON.parse(localStorage.getItem('token'));
    if (currentUser != null) {
      return currentUser.accessToken;
    }
  }

  public getRole(): string {
    let role = localStorage.getItem('role');
    return role;
  }
  public getIsAdmin() {
    return this.isAdmin;
  }

  public changeRole(role: boolean) {
    this.isAdmin = role;
  }

  public getIsAdminFromLocalStorage() {
    let role = localStorage.getItem('role');
    if (role === 'manager') {
      this.changeRole(true);
    }
  }
}
