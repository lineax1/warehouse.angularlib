import { Component, OnInit } from '@angular/core';
import { AccountView } from '../model/account/account.view';
import { AccountService } from '../services/account.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  public account: AccountView;
  public formGroup: FormGroup;
  public selectedFile: File;
  public stringUrl: string;
  public file: any;
  public accountId: number;

  constructor(
    private toastr: ToastrService,
    private accountService: AccountService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.getAccountId();
    this.getById();

    this.formGroup = this.formBuilder.group({
      firstName: [''],
      lastName: [''],
      email: [''],
      picture: ['']
    });
  }
  public getAccountId(): number {
    return this.accountId = JSON.parse(localStorage.getItem('accountId'));
  }

  public getById() {
    this.accountService.getById(this.accountId).subscribe(
      (res: AccountView) => { this.account = res; });
  }

  public onFileChanged(event): void {
    this.selectedFile = event.target.files[0];
    let uploadData = new FormData();
    uploadData.append('image', this.selectedFile);
    if (uploadData == null) {
      this.toastr.error('Invalid Data');
    } else {
      this.accountService.uploadImage(uploadData).subscribe(res => {
        this.stringUrl = res;
      });
    }
  }

  public onSubmit() {

    if (this.formGroup.invalid) {
      return this.toastr.error('Invalid Data!');
    }
    this.account.id = this.accountId;
    if (this.formGroup.controls.firstName.value) {
      this.account.firstName = this.formGroup.controls.firstName.value;
    }
    if (this.formGroup.controls.lastName.value) {
      this.account.lastName = this.formGroup.controls.lastName.value;
    }
    if (this.formGroup.controls.email.value) {
    this.account.email = this.formGroup.controls.email.value;
    }
    if (this.stringUrl) {
      this.account.picture = this.stringUrl;
    }

    this.accountService.update(this.account).subscribe();
    return this.toastr.success('Account Create');
  }

  get f() {
    return this.formGroup.controls;
  }
}
