import { Component, OnInit } from '@angular/core';
import { OrderService } from '../services/order.service';
import { ProductService } from '../services/product.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { ProductView } from '../model/product/product.view';
import { OrderView, OrderProduct } from '../model/order/order.view';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  public orderTotalPrice: number;
  public productList: ProductView[] = [];
  public formGroup: FormGroup;
  public order: OrderView = new OrderView();

  constructor(
    private orderService: OrderService,
    private formBuilder: FormBuilder,
    private productService: ProductService,
    private toastr: ToastrService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      address: ['', Validators.required],
      delivery: ['']
    });
  }

  get f() {
    return this.formGroup.controls;
  }

  public onSubmit() {
    if (this.formGroup.invalid) {
      this.toastr.error('Invalid Data');
    }
    this.order.address = this.formGroup.controls.address.value;
    this.order.delivery = this.formGroup.controls.delivery.value;
    this.getItem(this.order);
    this.orderService.create(this.order).subscribe();
    this.toastr.success('Succes Order');
    //this.router.navigate(['/product']);
  }

  public getItem(order: OrderView) {
    const existingOrder: ProductView[] = JSON.parse(
      localStorage.getItem('item')
    );
    order.productIdList = existingOrder.map((prod: ProductView) => {
      return new OrderProduct(prod.Id, prod.Quantity);
    });
    return order;
  }
}
