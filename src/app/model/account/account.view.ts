export class AccountView {
  public id: number;
  public firstName: string;
  public lastName: string;
  public email: string;
  public cash: number;
  public picture: string;
  public userList: Array<number>;
  constructor() {
    this.userList = new Array<number>();
  }
}
