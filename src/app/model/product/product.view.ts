import { ShipmentView } from '../shipment/shipment.view';

export class ProductView {
  public id: number;
  public name: string;
  public price: number;
  public SKU: string;
  public typeProduct: string;
  public shipment: ShipmentView;
  public quantity = 1;
}
