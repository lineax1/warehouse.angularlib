import { ShipmentView } from '../shipment/shipment.view';

export class OrderView {
  public id: number;
  public address: number;
  public delivery: string;
  public productIdList: OrderProduct[] = [];
}
export class OrderProduct {
  public id: number;
  public quantity: number;

  constructor(id: number, quantity: number) {
    this.id = id;
    this.quantity = quantity;
  }
}
