export class ShipmentView {
  public statusDescription: string;
  public orderStatus: string;
  public delivery: string;
  public productList: Array<number>;
  constructor() {
    this.productList = new Array<number>();
  }
}
