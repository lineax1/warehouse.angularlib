import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ProductService } from '../services/product.service';
import { ProductView } from '../model/product/product.view';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit {
  public productList: ProductView[] = [];
  public basketIsEmpty = false;
  public counter = 0;
  public orderTotalPrice = 0;
  public orderProductList: ProductView[] = [];
  constructor(
    public toastr: ToastrService,
    private dialogRef: MatDialogRef<BasketComponent>,
    private productService: ProductService
  ) { }

  ngOnInit(): void {
    this.showItem();
  }

  public loadData() {
    this.productService.getAll().subscribe((res: ProductView[]) => {
      this.productList = res;
      this.showItem();
    });
  }

  public showItem() {
    this.orderProductList = JSON.parse(
      localStorage.getItem('item')
    )
    if (this.orderProductList === null) {
      this.basketIsEmpty = true;
    }
    for (let i = 0; i < this.orderProductList.length; i++) {
      this.orderTotalPrice = this.orderTotalPrice + this.orderProductList[i].Price * this.orderProductList[i].Quantity;
    }
  }

  public deleteItem() {
    let key = "item";
    localStorage.removeItem(key);
    window.location.reload();
    this.toastr.success("removed");
  }
}
