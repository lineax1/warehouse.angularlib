import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { BasketComponent } from '../basket/basket.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  public manager = false;

  constructor(
    private userService: UserService,
    private router: Router,
    private dialog: MatDialog,
    ) {
  }
  ngOnInit() {
    this.manager = this.userService.getIsAdmin();
  }

  public Logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    localStorage.removeItem('item');
    localStorage.removeItem('accountId');
    this.router.navigate(['/login']);
  }

  public openAdminPage() {
    this.router.navigate(['/admin-page']);
  }

  public openBasket(): void {
    const dialogRef = this.dialog.open(BasketComponent);
    dialogRef.afterClosed().subscribe();
}
}
