import { Component, OnInit } from '@angular/core';
import { UserView } from '../model/user/user.view';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../services/user.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public user: UserView;
  public formGroup: FormGroup;
  constructor(
    private loginService: UserService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private route: Router
  ) {

  }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      login: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  get f() {
    return this.formGroup.controls;
  }

  public onSubmit() {
    if (this.formGroup.invalid) {
      return;
    }
    const user: UserView = new UserView();
    user.login = this.formGroup.controls.login.value;
    user.password = this.formGroup.controls.password.value;

    this.loginService.login(user).subscribe(result => {
      if (result) {
        localStorage.setItem('token', JSON.stringify(result.token));
        localStorage.setItem('role', result.role);
        this.route.navigate(['/account']);
      } else { }
    },
      error => {
        if (error) {
          return this.toastr.error('Invalid Data!!');
        }
      });
  }
}
