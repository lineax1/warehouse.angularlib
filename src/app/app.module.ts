import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { AppComponent } from './app.component';
import { ProductComponent } from './product/product.component';
import { ShipmentComponent } from './shipment/shipment.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { AccountService } from './services/account.service';
import { ProductService } from './services/product.service';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from './services/user.service';
import { ShipmentService } from './services/shipment.service';
import { OrderService } from './services/order.service';
import { Routes, RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AccountComponent } from './account/account.component';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AdminPanelModule } from './admin-panel/adminpanel.module';
import { SharedModule } from './common/shared.modul';
import { BasketComponent } from './basket/basket.component';
import { OrderComponent } from './order/order.component';


const routes: Routes = [
  { path: 'product', component: ProductComponent },
  { path: 'shipment', component: ShipmentComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: 'login', component: LoginComponent },
  { path: 'account', component: AccountComponent },
  { path: 'basket', component: BasketComponent },
  { path: 'order', component: OrderComponent },
  { path: '**', redirectTo: 'product' }
];

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    ShipmentComponent,
    RegistrationComponent,
    LoginComponent,
    AccountComponent,
    BasketComponent,
    OrderComponent,
  ],
  imports: [
    SharedModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    MDBBootstrapModule,
    MatCardModule,
    MatDialogModule,
    AdminPanelModule,
    MatMenuModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    }),
    RouterModule.forRoot(routes)
  ],
  providers: [AccountService, OrderService, ShipmentService, UserService, ProductService,
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: [] }],
  bootstrap: [AppComponent]
})
export class AppModule { }
