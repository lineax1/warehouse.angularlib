import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { CreateProductComponent } from './create-product/create-product.component';
import { EditOrderComponent } from './edit-order/edit-order.component';
import { SharedModule } from '../common/shared.modul';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { UpdateProductComponent } from './update-product/update-product.component';



@NgModule({
  declarations: [
    AdminPageComponent,
    EditOrderComponent,
    CreateProductComponent,
    EditProductComponent,
    UpdateProductComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: 'admin-page',
        component: AdminPageComponent,
        children: [
          {
            path: 'edit-product',
            component: EditProductComponent,
            outlet: 'editing'
          },
          {
            path: 'create-product',
            component: CreateProductComponent,
            outlet: 'editing'
          },
          {
            path: 'edit-order',
            component: EditOrderComponent,
            outlet: 'editing'
          }
        ]
      }]),
    SharedModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports: [RouterModule]
})
export class AdminPanelModule { }
