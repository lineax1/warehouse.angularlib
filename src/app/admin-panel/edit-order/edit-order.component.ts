import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { CreateProductComponent } from '../create-product/create-product.component';
import { OrderService } from 'src/app/services/order.service';
import { OrderView } from 'src/app/model/order/order.view';
import { ProductService } from 'src/app/services/product.service';
import { ProductView } from 'src/app/model/product/product.view';

@Component({
  selector: 'app-edit-order',
  templateUrl: './edit-order.component.html',
  styleUrls: ['./edit-order.component.css']
})
export class EditOrderComponent implements OnInit {
  public orderList: OrderView[] = [];
  public productList: ProductView[] = [];
  public formGroup: FormGroup;
  constructor(
    private productService: ProductService,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<CreateProductComponent>,
    private orderService: OrderService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    this.getAll();
    this.getAllProduct();
  }
  public getAll(): void {
    this.orderService.getAll().subscribe(
      (res: OrderView[]) => { this.orderList = res; });
  }
  public getAllProduct() {
    this.productService.getAll().subscribe(
      (res: ProductView[]) => { this.productList = res; });
  }

  public delete(id: number) {
    this.orderService.delete(id).subscribe(
       () => this.getAll());
    this.toastr.success('Order Deleted');
  }

  public getProductList(order: { productList: { name: string }[] }) {
    let productString = "" ;
    for (let i = 0; i < order.productList.length; i++) {
      productString += order.productList[i].name + ",";
    }
    return productString;
  }
}
