import { Component, OnInit } from '@angular/core';
import { ProductView } from 'src/app/model/product/product.view';
import { ProductService } from 'src/app/services/product.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CreateProductComponent } from '../create-product/create-product.component';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {
  public isOpenProductView = false;
  public productList: ProductView[] = [];
  public isOpenOrderView = false;

  constructor(
    private productService: ProductService,
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<CreateProductComponent>,
  ) { }

  ngOnInit(): void {
  }
  public onClickProductButton() {
    this.isOpenProductView = !this.isOpenProductView;
  }
  public onClickOrderButton() {
    this.isOpenOrderView  = !this.isOpenOrderView;
  }
  public getAll(): void {
    this.productService.getAll().subscribe(
      (res: ProductView[]) => { this.productList = res; });
  }
  public openCreateProduct(): void {
    const dialogRef = this.dialog.open(CreateProductComponent);
    dialogRef.afterClosed().subscribe(res => {
      this.getAll();
    });
  }
}
