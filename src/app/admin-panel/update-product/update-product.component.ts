import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { ProductView } from 'src/app/model/product/product.view';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductService } from 'src/app/services/product.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit {
  public matDialog: MatDialog;
  public formGroup: FormGroup;
  public product: ProductView;
  @Output() submitForm: EventEmitter<FormGroup> = new EventEmitter<FormGroup>();
  constructor(
    private productService: ProductService,
    private toastr: ToastrService,
    public formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<UpdateProductComponent>
  ) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      SKU: ['', Validators.required]
    });
    this.submitForm.emit(this.formGroup);
  }

  public onSubmit() {
    if (this.formGroup.invalid) {
      this.toastr.error('Invalid Data');
    }
    this.product.name = this.formGroup.controls.name.value;
    this.product.SKU = this.formGroup.controls.SKU.value;
    this.product.price = this.formGroup.controls.price.value;
    this.productService.update(this.product).subscribe();
    this.dialogRef.close();
    this.toastr.success('Product Create');
  }
  get f() {
    return this.formGroup.controls;
  }

}
