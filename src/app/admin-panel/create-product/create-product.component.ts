import { Component, OnInit, Inject } from '@angular/core';
import { ProductView } from 'src/app/model/product/product.view';
import { FormBuilder, FormGroup, Validators, } from '@angular/forms';
import {  MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})

export class CreateProductComponent implements OnInit {
  public product: ProductView;
  public formGroup: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private productService: ProductService,
    public dialogRef: MatDialogRef<CreateProductComponent>
  ) {}
  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      SKU: ['', Validators.required],
      typeProduct: ['', Validators.required]
    });
  }

  public onSubmit() {
    if (this.formGroup.invalid) {
      this.toastr.error('Invalid Data');
    }
    const product: ProductView = new ProductView();
    product.name = this.formGroup.controls.name.value;
    product.SKU = this.formGroup.controls.SKU.value;
    product.price = this.formGroup.controls.price.value;
    product.typeProduct = this.formGroup.controls.typeProduct.value;
    this.productService.create(product).subscribe();
    this.dialogRef.close();
    this.toastr.success('Product Create');
  }
  get f() {
    return this.formGroup.controls;
  }
}
