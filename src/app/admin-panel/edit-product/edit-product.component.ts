import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ProductView } from 'src/app/model/product/product.view';
import { ProductService } from 'src/app/services/product.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { CreateProductComponent } from '../create-product/create-product.component';
import { UpdateProductComponent } from '../update-product/update-product.component';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  public productList: ProductView[] = [];
  public product: ProductView;
  @Output() submitForm = new EventEmitter<FormGroup>();
  public formGroup: FormGroup;
  constructor(
    private productService: ProductService,
    private toastr: ToastrService,
    public formBuilder: FormBuilder,
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<CreateProductComponent>,
  ) { }

  ngOnInit(): void {
    this.getAll();
    this.formGroup = this.formBuilder.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      SKU: ['', Validators.required],
      productType: ['', Validators.required]
    });
  }

  public getAll(): void {
    this.productService.getAll().subscribe(
      (res: ProductView[]) => { this.productList = res; });
  }

  public deleteProduct(id: number) {
    this.productService.delete(id).subscribe(
      res => () => this.getAll());
    this.toastr.success('Products Deleted');
  }

  public openUpdateProduct(product): void {
    const dialogRef = this.dialog.open(UpdateProductComponent);
    dialogRef.componentInstance.product = product;
    dialogRef.afterClosed().subscribe(res => {
      this.getAll();
    });
  }

  get f() {
    return this.formGroup.controls;
  }
}
