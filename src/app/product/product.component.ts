import { Component, OnInit } from '@angular/core';
import { ProductView } from '../model/product/product.view';
import { ProductService } from '../services/product.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  public productList: ProductView[] = [];
  public product: ProductView;
  constructor(
    private productService: ProductService,
    private toastr: ToastrService,
  ) { }

  public ngOnInit(): void {
    this.getAll();
  }

  public getAll(): void {
    this.productService.getAll().subscribe((res: ProductView[]) => {
      this.productList = res;
    });
  }

  public addToOrder(product) {
    let orderList: ProductView[] = [];
    const key = 'item';
    orderList = JSON.parse(localStorage.getItem(key));
    if (!orderList) {
      orderList = new Array<ProductView>();
    }
    product.Quantity = 1;
    let filteraddProducts = orderList.filter(item => item.Id === product.Id);
    if (filteraddProducts.length) {
       filteraddProducts[0].Quantity = filteraddProducts[0].Quantity + 1;
    }
    else{
      orderList.push(product);
    }
    localStorage.setItem(key, JSON.stringify(orderList));
    this.toastr.success('Product Add To Order');
  }
}
